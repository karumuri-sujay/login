import './App.css';
import Login from './Login/Login';
import { BrowserRouter as Router, Switch,Route,Link} from "react-router-dom";
import HospitalIndex from './Hospital/HospitalIndex';
import PasswordReset from './forgot/PasswordReset';

function App() {
  return (
    <div>
      <Router>
          <Link to="/hospital"></Link>
          <Link to="/login"></Link>
          <Link to="/forgot"></Link>
        <Switch>
          <Route path="/hospital">
            <HospitalIndex/>
          </Route>
          <Route exact path="/">
            <Login/>
          </Route>
          <Route path="/forgot">
            <PasswordReset/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
