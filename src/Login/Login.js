import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import logo from './logo.png';
import brand from './brand1.png';
import './Login.css';


function Login(){
    let [user,setName]=useState("");
    let [drop,setDrop]=useState("");
    let [password,setPassword]=useState("");

    function onSubmit(e){
        console.log(user+" "+drop+" "+password);
        e.preventDefault();
    }
    /*let toggle=()=>{
        var pswrd = document.getElementById('pswrd');
        var icon = document.querySelector('.fas');
        if (pswrd.type === "password") {
            pswrd.type = "text";
            pswrd.style.marginTop = "20px";
            icon.style.color = "#7f2092";
        }
        else{
            pswrd.type = "password";
            icon.style.color = "grey";
        }
    }*/
    return(
        <div>
            <div className="row">
                <div className="col-md-6">
                    <img className="image" alt="background" src={logo}/>
                </div>
                <div className="col-md-6 Alignment">
                    <div className="d-flex login-box">
                        <form onSubmit={onSubmit}>
                            <div className="form-div">
                                <h1 className="welcome">Welcome</h1>
                                <img src={brand} className="Logo-Brand" alt="Logo Brand"/>
                                <input type="text" required placeholder="User Name" value={user} onChange={e => setName(e.target.value)} className="input-text"/>
                                <select defaultValue={'DEFAULT'} className="dropdown" required onChange={e => setDrop(e.target.value)}>
                                    <option disabled selected value="DEFAULT">--Select Type--</option>
                                    <option value="Doctor">   Doctor</option>
                                    <option value="Pathologist">   Pathologist</option>
                                    <option value="Patient">   Patient</option>
                                </select>
                                <input id="pswrd" required type="password" value={password} onChange={e => setPassword(e.target.value)} placeholder="Password" className="input-pas"/>
                                {/* <span>
                                    <i class="fa fa-eye" aria-hidden="true" id="eye" onclick="toggle()"></i>
                                </span> */}
                                <small className="new-user"><a href="/newUser">New User?</a></small>
                                <small className="forgot"><a href="/forgot">Forgot Password?</a></small>
                                {/* <br></br> */}
                                <button type="submit" className="Login-button">LOG IN</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;