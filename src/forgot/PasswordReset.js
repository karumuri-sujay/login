import React, { Component } from 'react';
import logo from './logo.png';
import 'bootstrap/dist/css/bootstrap.min.css';
import './PasswordReset.css';

function PasswordReset(){
    return(
        <div>
            <div className="row">
                <div className="col-md-6">
                    <img className="image" alt="background" src={logo}/>
                </div>
                <div className="col-md-6 Alignment">
                    <div className="d-flex reset-box">
                        <form>
                            <div className="form-div">
                                <h1 className="reset">Password Reset</h1>
                                <small className="mail-details">Enter your email address and we will send you reset link</small>
                                <input type="mail" name="mail-id" placeholder="Email" className="mail-text"/>
                                <button type="submit" className="Login-button">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PasswordReset;