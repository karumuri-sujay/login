import React, { Component, useState } from 'react';
import './Content.css';
import logo from './schedule.png';
import doctors from './Group 253.png';
import progress from './Group 254.png';
import complete from './Group 255.png';
import archive from './Group 324.png';
import deleted from './Group 323.png';
import FacultyInfo from '../Information/FacultyInfo';
import PatientsInfo from '../Information/PatientsInfo';
import CasesInfo from '../Information/CasesInfo';
import back from './sideNav/back.svg';
import feedback from './sideNav/feedback.svg';
import forward from './sideNav/forward.svg';
import help from './sideNav/help.svg';
import menu from './sideNav/menu.png';

function HospitalContent(){
    let [display,setDisplay]=useState(0);
    const Faculty=()=>{
        setDisplay(display=0);
        // console.log(display);
        // console.log(typeof display);
    }

    const Patients=()=>{
        setDisplay(display=1);
    }

    const Cases=()=>{
        setDisplay(display=2);
    }

    const [sidemenu, setSideMenu] = useState(0);

    const DisplayMenu = () => {
        sidemenu === 0 ? setSideMenu(1) : setSideMenu(0);
    };

    return(
        <div>
            <div className="row">
                <div className="col-md-2">
                    <div className={sidemenu === 0 ? "sidebar" : "sidebar-extend"}>
                        <div className="side-div">
                        <img src={menu} id="menubtn" onClick={DisplayMenu} alt="menu" />
                        <img src={back} className="side-icon" alt="back" />
                        <img src={forward} className="side-icon" alt="forward" />
                        <img src={feedback} className="side-icon" alt="menu" />
                        <img src={help} className="side-icon" alt="menu" />
                    </div>
                    {sidemenu === 1 ? (
                    <div className="side-nav">
                        <text className="side-nav-title">Back</text>
                        <text className="side-nav-title">Forward</text>
                        <text className="side-nav-title">Feedback</text>
                        <text className="side-nav-title">Info</text>
                    </div>
                    ) : (<></>
                    )}
                </div>
            </div>
                <div className="col-md-10">
                    <div className="scheduled-1">
                        <img src={logo} title="All Cases" alt="schedule" className="btn-icons" />
                        <div className="div-btn">
                            <text className="btn-data">12</text>
                            <text className="btn-title">All Cases</text>
                        </div>
                    </div>
                    <div className="scheduled-2">
                        <img src={doctors} alt="schedule" className="btn-icons" />
                        <div className="div-btn">
                            <text className="btn-data">05</text>
                            <text className="btn-title">Doctors</text>
                        </div>
                    </div>
                    <div className="scheduled-3">
                        <img src={progress} alt="schedule" className="btn-icons" />
                        <div className="div-btn">
                            <text className="btn-data">25</text>
                            <text className="btn-title">Progress</text>
                        </div>
                    </div>
                    <div className="scheduled-4">
                        <img src={complete} alt="schedule" className="btn-icons" />
                        <div className="div-btn">
                            <text className="btn-data">129</text>
                            <text className="btn-title">Completed</text>
                        </div>
                    </div>
                    <div className="scheduled-5">
                        <img src={archive} alt="schedule" className="btn-icons" />
                        <div className="div-btn">
                            <text className="btn-data">18</text>
                            <text className="btn-title">Archived</text>
                        </div>
                    </div>
                    <div className="scheduled-6">
                        <img src={deleted} alt="schedule" className="btn-icons" />
                        <div className="div-btn">
                            <text className="btn-data">80</text>
                            <text className="btn-title">Deleted</text>
                        </div>
                    </div>
                    <div className="details">
                        <ul className="items">
                            <li><button type="button" onClick={Faculty}>Faculty</button></li>
                            <li><button type="button" onClick={Patients}>Patients</button></li>
                            <li><button type="button" onClick={Cases}>Cases</button></li>
                        </ul>
                        {/* Details for the corresponding type */}
                        {display===0 ? <FacultyInfo/> : display===1 ? <PatientsInfo/> : display===2 ? <CasesInfo/> : <FacultyInfo/> }
                    </div>
                </div>
            </div>
            {/* <div className="details">
                <ul className="items">
                    <li><button type="button" onClick={Faculty}>Faculty</button></li>
                    <li><button type="button" onClick={Patients}>Patients</button></li>
                    <li><button type="button" onClick={Cases}>Cases</button></li>
                </ul> */}
                {/* Details for the corresponding type */}
                {/* {display===0 ? <FacultyInfo/> : display===1 ? <PatientsInfo/> : display===2 ? <CasesInfo/> : <FacultyInfo/> } */}
            </div>
        // </div>
    )
}

export default HospitalContent;