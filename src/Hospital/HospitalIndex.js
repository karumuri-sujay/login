import React, { Component } from 'react';
import HospitalContent from './Content/HospitalContent';
import HospitalNav from './HospitalNav';

function HospitalIndex(){
    return(
        <div>
            <HospitalNav/>
            <HospitalContent/>
        </div>
    )
}

export default HospitalIndex;