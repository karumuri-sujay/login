import React, { Component, useState } from 'react';
import './TableDesign.css';
import {Modal, Button, Row, Col, Form} from 'react-bootstrap';
import logo from './schedule.png';
import { FormGroup } from 'react-bootstrap';
import { FormLabel } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import { FormCheck } from 'react-bootstrap';

function MyVerticallyCenteredModal(props) {
    return (
        <Modal
            {...props}
            size="lg"
            dialogClassName="my-modal"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
        <Modal.Header style={{backgroundColor:'#91007C'}} closeButton>
            <Modal.Title style={{color:'white'}} id="contained-modal-title-vcenter">
            Add/Edit Faculty Details
            </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{marginRight:'80px'}}>
            <Form>
                <Row>
                    <Col></Col>
                    <Col>
                        <input type="file" accept=".png" label="Upload" className="file-upload"/>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right',margin:'auto',display:'block'}}>Full Name</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control style={{marginBottom:'20px',border:'1px solid #C740A9',padding:'10px'}} placeholder="Full Name"></Form.Control>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Age</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control style={{marginBottom:'20px',border:'1px solid #C740A9',width:'150px'}} placeholder="Age"></Form.Control>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Gender</Form.Label>
                    </Col>
                    <Col>
                    {[ 'radio'].map((type) => (
                        <div key={`inline-${type}`} className="mb-3">
                            <Form.Check
                                inline
                                label="male"
                                name="group1"
                                color='#C740A9'
                                type={type}
                                id={`inline-${type}-1`}
                            />
                            <Form.Check
                                inline
                                label="Female"
                                name="group1"
                                type={type}
                                id={`inline-${type}-2`}
                            />
                        </div>
                    ))}
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Specialization</Form.Label>
                    </Col>
                    <Col>
                        <Form.Select style={{marginBottom:'20px',border:'1px solid #C740A9'}} placeholder="specialization">
                            <option selected hidden disabled>Specialization</option>
                            <option>s1</option>
                            <option>S2</option>
                            <option>S3</option>
                        </Form.Select>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Department</Form.Label>
                    </Col>
                    <Col>
                    <Form.Select style={{marginBottom:'20px',border:'1px solid #C740A9'}} placeholder="specialization">
                            <option selected hidden disabled>Department</option>
                            <option>s1</option>
                            <option>S2</option>
                            <option>S3</option>
                        </Form.Select>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Registration ID</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control style={{marginBottom:'20px',border:'1px solid #C740A9'}} placeholder="Registration ID"></Form.Control>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Email</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control style={{marginBottom:'20px',border:'1px solid #C740A9'}} placeholder="Email"></Form.Control>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Contact Number</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control style={{marginBottom:'20px',border:'1px solid #C740A9'}} placeholder="Contact Number"></Form.Control>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Type</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control style={{marginBottom:'20px',border:'1px solid #C740A9'}} placeholder="Type"></Form.Control>
                    </Col>
                </Row>
                <Row>
                    <Col></Col>
                    <Col>
                        <Button style={{float:'right',backgroundColor:'#C740A9',marginTop:'20px'}} type="submit">Save</Button>
                        <Button style={{marginRight:'10px',float:'right',marginTop:'20px',color:'#C740A9',border:'1px solid #C740A9',backgroundColor:'white',marginBottom:'40px'}} onClick={props.onHide}>Cancel</Button>
                    </Col>
                </Row>
            </Form>
        </Modal.Body>
        {/* <Modal.Footer>
            <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer> */}
        </Modal>
    );
}

function FacultyInfo(){
    const [modalShow,setModalShow]=useState(false);

    let [data,setData]=useState({
        ID:125,
        name:"Justine",
        spec:"Pathology",
        contact:"9219834028",
        dept:"Gastroenterology",
    })
    return(
        <div>
            <div className="table-box">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Specialization</th>
                            <th>Contact</th>
                            <th>Department</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>125</td>
                            <td>Justine</td>
                            <td>Pathology</td>
                            <td>9219834028</td>
                            <td>Gastroenterology</td>
                            <td><button onClick={()=>setModalShow(true)} type="button">Edit</button></td>
                        </tr>
                        <tr>
                            <td>125</td>
                            <td>Justine</td>
                            <td>Pathology</td>
                            <td>9219834028</td>
                            <td>Gastroenterology</td>
                            <td><button onClick={()=>setModalShow(true)} type="button">Edit</button></td>
                        </tr>
                        <tr>
                            <td>125</td>
                            <td>Justine</td>
                            <td>Pathology</td>
                            <td>9219834028</td>
                            <td>Gastroenterology</td>
                            <td><button onClick={()=>setModalShow(true)} type="button">Edit</button></td>
                        </tr>
                        <tr>
                            <td>125</td>
                            <td>Justine</td>
                            <td>Pathology</td>
                            <td>9219834028</td>
                            <td>Gastroenterology</td>
                            <td><button onClick={()=>setModalShow(true)} type="button">Edit</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className="add-doctor">
                <button onClick={()=>setModalShow(true)} type="button"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Doctor</button>
            </div>
            <MyVerticallyCenteredModal show={modalShow} onHide={() => setModalShow(false)}/>
        </div>
    )
}

export default FacultyInfo;