import React, { Component, useState } from 'react';
import './Devices.css';
import {Modal, Button, Row, Col, Form} from 'react-bootstrap';
import { FormGroup } from 'react-bootstrap';
import { FormLabel } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import { FormCheck } from 'react-bootstrap';

function MyVerticallyCenteredModal(props) {
    return (
        <Modal
            {...props}
            size="lg"
            dialogClassName="my-modal"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
        <Modal.Header style={{backgroundColor:'#91007C'}} closeButton>
            <Modal.Title style={{color:'white'}} id="contained-modal-title-vcenter">
                Edit Patients
            </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{marginRight:'80px',marginTop:'30px'}}>
            <Form>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right',margin:'auto',display:'block'}}>Full Name</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control style={{marginBottom:'20px',border:'1px solid #C740A9',padding:'10px'}} placeholder="Full Name"></Form.Control>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Age</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control style={{marginBottom:'20px',border:'1px solid #C740A9',width:'150px'}} placeholder="Age"></Form.Control>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Gender</Form.Label>
                    </Col>
                    <Col>
                    {[ 'radio'].map((type) => (
                        <div key={`inline-${type}`} className="mb-3">
                            <Form.Check
                                inline
                                label="male"
                                name="group1"
                                color='#C740A9'
                                type={type}
                                id={`inline-${type}-1`}
                            />
                            <Form.Check
                                inline
                                label="Female"
                                name="group1"
                                type={type}
                                id={`inline-${type}-2`}
                            />
                        </div>
                    ))}
                    </Col>
                </Row>
                {/* <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Specialization</Form.Label>
                    </Col>
                    <Col>
                        <Form.Select style={{marginBottom:'20px',border:'1px solid #C740A9'}} placeholder="specialization">
                            <option selected hidden disabled>Specialization</option>
                            <option>s1</option>
                            <option>S2</option>
                            <option>S3</option>
                        </Form.Select>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Department</Form.Label>
                    </Col>
                    <Col>
                    <Form.Select style={{marginBottom:'20px',border:'1px solid #C740A9'}} placeholder="specialization">
                            <option selected hidden disabled>Department</option>
                            <option>s1</option>
                            <option>S2</option>
                            <option>S3</option>
                        </Form.Select>
                    </Col>
                </Row> */}
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Address</Form.Label>
                    </Col>
                    <Col>
                        <Form.Group className="mb-3" controlId="formGridAddress1">
                            <Form.Control placeholder="Address" style={{border:'1px solid #C740A9',marginBottom:'20px'}}/>
                            {/* <Form.Control placeholder="Address" style={{border:'1px solid #C740A9'}}/> */}
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Email</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control style={{marginBottom:'20px',border:'1px solid #C740A9'}} placeholder="Email"></Form.Control>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label style={{float:'right'}}>Contact Number</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control style={{marginBottom:'20px',border:'1px solid #C740A9'}} placeholder="Contact Number"></Form.Control>
                    </Col>
                </Row>
                <Row>
                    <Col></Col>
                    <Col>
                        <Button style={{float:'right',backgroundColor:'#C740A9',marginTop:'20px'}} type="submit">Submit</Button>
                        <Button style={{marginRight:'10px',float:'right',marginTop:'20px',color:'#C740A9',border:'1px solid #C740A9',backgroundColor:'white',marginBottom:'40px'}} onClick={props.onHide}>Cancel</Button>
                    </Col>
                </Row>
            </Form>
        </Modal.Body>
        {/* <Modal.Footer>
            <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer> */}
        </Modal>
    );
}

function PatientsInfo(){
    const [modalShow,setModalShow]=useState(false);

    return(
        <div>
            <div className="table-box">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Gender</th>
                            <th>Status</th>
                            <th>Pending</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>125</td>
                            <td>Glenn Phillips</td>
                            <td>24</td>
                            <td>M</td>
                            <td>Discharged</td>
                            <td>53458</td>
                            <td><button onClick={()=>setModalShow(true)} type="button">Edit</button></td>
                        </tr>
                        <tr>
                            <td>125</td>
                            <td>Glenn Phillips</td>
                            <td>24</td>
                            <td>M</td>
                            <td>Discharged</td>
                            <td>53458</td>
                            <td><button onClick={()=>setModalShow(true)} type="button">Edit</button></td>
                        </tr>
                        <tr>
                            <td>125</td>
                            <td>Glenn Phillips</td>
                            <td>24</td>
                            <td>M</td>
                            <td>Discharged</td>
                            <td>53458</td>
                            <td><button onClick={()=>setModalShow(true)} type="button">Edit</button></td>
                        </tr>
                        <tr>
                            <td>125</td>
                            <td>Glenn Phillips</td>
                            <td>24</td>
                            <td>M</td>
                            <td>Discharged</td>
                            <td>53458</td>
                            <td><button onClick={()=>setModalShow(true)} type="button">Edit</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <MyVerticallyCenteredModal show={modalShow} onHide={() => setModalShow(false)}/>
        </div>
    )
}

export default PatientsInfo;