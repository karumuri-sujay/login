import React, { Component } from 'react';
import './Devices.css';

function CasesInfo(){
    return(
        <div>
            <div className="table-box">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Patient ID</th>
                            <th>Name</th>
                            <th>Dr. Name</th>
                            <th>Status</th>
                            <th>Report</th>
                            <th>Full Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>125</td>
                            <td>53485</td>
                            <td>Glenn Phillips</td>
                            <td>Dr. S Kalam</td>
                            <td>In Progress</td>
                            <td>Download</td>
                            <td><button type="button">Full Details</button></td>
                        </tr>
                        <tr>
                            <td>125</td>
                            <td>53485</td>
                            <td>Glenn Phillips</td>
                            <td>Dr. S Kalam</td>
                            <td>In Progress</td>
                            <td>Download</td>
                            <td><button type="button">Full Details</button></td>
                        </tr>
                        <tr>
                            <td>125</td>
                            <td>53485</td>
                            <td>Glenn Phillips</td>
                            <td>Dr. S Kalam</td>
                            <td>In Progress</td>
                            <td>Download</td>
                            <td><button type="button">Full Details</button></td>
                        </tr>
                        <tr>
                            <td>125</td>
                            <td>53485</td>
                            <td>Glenn Phillips</td>
                            <td>Dr. S Kalam</td>
                            <td>In Progress</td>
                            <td>Download</td>
                            <td><button type="button">Full Details</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default CasesInfo;